package org.devops

//saltstack
def saltDeploy(hosts,func) {
    sh "salt \"${hosts}\" ${func}"
}

//ansible
def ansibleDeploy(hosts,cmdParameter,func) {
    sh "ansible ${hosts} ${cmdParameter} ${func} "
}